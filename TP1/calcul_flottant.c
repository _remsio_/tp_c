#include <stdio.h>
#include <string.h>

int main() {

  float X;
  int X_entier;

  X = 10 / 3;
  printf("opération 1 [X = 10 / 3 = %f] \n", X);
  printf("Le résultat est un flottant, mais les 2 opérandes étant des entiers, le résultat ne possède pas de partie décimal\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  X_entier = 10 / 3;
  printf("opération 2 [X_entier = 10 / 3 = %d] \n", X_entier);
  printf("Le résultat est le quotient du résultat de l'opération\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  X = 10.0 / 3.0;
  printf("opération 3 [X = 10.0 / 3.0 = %f] \n", X);
  printf("Le résultat est un flottant arrondi à 6 chiffres après la virugule\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  X = (float) 10 / 3;
  printf("opération 4 [X = (float) 10 / 3 = %f] \n", X);
  printf("Le résultat est un flottant arrondi à 6 chiffres après la virugule\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  X = ((float) 10) / 3;
  printf("opération 5 [X = ((float) 10) / 3 = %6.1f] \n", X);
  printf("Le résultat est un flottant arrondi à 5 chiffres après la virugule\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  X = ((float) 10) / 3;
  printf("opération 6 [X = ((float) 10) / 3 = %6.5f] \n", X);
  printf("Le résultat est un flottant arrondi à 1 chiffre après la virugule\n");
  return 0;
}
