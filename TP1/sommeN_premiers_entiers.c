#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

  if(argc != 2)
  {
    fprintf(stderr, "%s", "Ce programme attend uniquement une entrée!\n");
    return(1);
  }
  printf("Boucle for incrémentée\n");

  int n = atoi(argv[1]);
  int resultat = 0;
  for(int i=0; i <= n; i++)
  {
    resultat += i;
  }
  printf("Résultat incrémenté : %d\n", resultat);

  resultat = 0;
  printf("Boucle for décrémentée\n");
  for(int i=n; i > 0; i--)
  {
    resultat += i;
  }
  printf("Résultat décrémenté : %d\n", resultat);

  resultat = 0;
  int i = 0;
  printf("Boucle while incrémentée\n");
  while(i <= n)
  {
    resultat += i;
    i ++;
  }
  printf("Résultat while incrémenté : %d\n", resultat);

  resultat = 0;
  i= n;
  printf("Boucle while décrémentée\n");
  while(i > 0)
  {
    resultat += i;
    i--;
  }
  printf("Résultat while décrémenté : %d\n", resultat);

  return(0);
}
