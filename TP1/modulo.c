#include <stdio.h>
#include <string.h>

int main() {

  int X;
  X = 5 % 2;
  printf("opération 1 [X = 5 %% 2 = %d] \n", X);
  printf("Le résultat est bien celui attendu\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  //X = 5.0 % 2;
  printf("opération 2 [X = 5.0 %% 2 = IMPOSSIBLE] \n");
  printf("Cette opération est impossible, l'operation modulo attend 2 entiers et renvoie un modulo\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  return 0;
}
