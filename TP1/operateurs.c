#include <stdio.h>
#include <string.h>

int main() {

  int X;

  X = -3 + 4 * 5 - 6;
  printf("opération 1 [X = -3 + 4 * 5 - 6 = %d] \n", X);
  X = (-3 + 4) * 5 - 6;
  printf("opération 2 [X = (-3 + 4) * 5 - 6 = %d] \n", X);
  X = -3 + (4 * 5) - 6;
  printf("opération 3 [X = -3 + (4 * 5) - 6 = %d] \n", X);
  X = -3 + 4 * (5 - 6);
  printf("opération 4 [X = -3 + 4 * 5 - 6 = %d] \n", X);
  return 0;
}
