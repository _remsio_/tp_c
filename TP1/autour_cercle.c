#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

  if(argc != 2)
  {
    fprintf(stderr, "%s", "Ce programme attend uniquement une entrée!\n");
    return(1);
  }

  const float PI = 3.14159;
  float rayon = atof(argv[1]);

  printf("rayon : %f\n", rayon);

  printf("perimetre : %f\n", 2 * PI * rayon);

  printf("surface : %f\n", PI * rayon * rayon);

  return(0);
}
