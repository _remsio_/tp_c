#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int pgcd(int nombre1, int nombre2)
{

  int result = nombre1;

  while(nombre1 != nombre2)
  {
    if(nombre1 > nombre2){
      result = nombre1 - nombre2;
      nombre1 = result;
    }else{
      result = nombre2 - nombre1;
      nombre2 = result;
    }
  }

  return result;
}

int main(int argc, char *argv[]) {

  char saisie[] = "";
  printf("nombre1 : ");
  fgets(saisie, 10, stdin);
  int nombre1 = atoi(saisie);

  printf("nombre2 : ");
  fgets(saisie, 10, stdin);
  int nombre2 = atoi(saisie);

  printf("pgcd(%d, %d) = %d\n", nombre1, nombre2, pgcd(nombre1, nombre2));

  return(0);
}
