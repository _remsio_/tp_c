#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int xPuissanceN(int x, int n)
{

  int result = x;

  if(x == 0){
    return 1;
  }

  for(int i=1; i < n; i++){
    result *= x;
  }

  return result;
}

int main(int argc, char *argv[]) {

  char saisie[] = "";
  printf("x : ");
  fgets(saisie, 10, stdin);
  int x = atoi(saisie);
  if(x < 0)
  {
    fprintf(stderr, "%s", "x ne peut être négatif ou égal à 0\n");
  }

  printf("n : ");
  fgets(saisie, 10, stdin);
  int n = atoi(saisie);
  if(x <= 0)
  {
    fprintf(stderr, "%s", "n ne peut être négatif\n");
  }

  printf("xPuissanceN(%d, %d) = %d\n", x, n, xPuissanceN(x, n));

  return(0);
}
