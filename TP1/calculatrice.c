#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

  char saisie[] = "";
  char operande;

  printf("Vous pouvez quitter la calculatrice à tout moment en saisissant 's'\n");
  do {
    printf("opération (*,/,+,-) : ");
    fgets(saisie, 3, stdin);
    operande = saisie[0];

    if(saisie[0] == 's')
    {
      break;
    }

    if(saisie[0] != '*' && saisie[0] != '/' && saisie[0] != '+' && saisie[0] != '-')
    {
      fprintf(stderr, "%s", "Cette calculatrice ne supporte que les opérations suivantes : *, /, +, -\n");
      continue;
    }

    printf("nombre1 : ");
    fgets(saisie, 15, stdin);
    float nombre1 = (float) atof(saisie);

    if(saisie[0] == 's')
    {
      break;
    }

    printf("nombre2 : ");
    fgets(saisie, 15, stdin);
    float nombre2 = (float) atof(saisie);
    printf("\n");

    if(saisie[0] == 's')
    {
      break;
    }

    if(operande == '/' && nombre2 == 0.0){
      fprintf(stderr, "%s", "Impossible de diviser par 0!\n");
      continue;
    }

    switch (operande) {
      case '*':
      printf("%f * %f = %f\n", nombre1, nombre2, nombre1 * nombre2);
      break;
      case '/':
      printf("%f / %f = %f\n", nombre1, nombre2, nombre1 / nombre2);
      break;
      case '+':
      printf("%f + %f = %f\n", nombre1, nombre2, nombre1 + nombre2);
      break;
      case '-':
      printf("%f - %f = %f\n", nombre1, nombre2, nombre1 - nombre2);
      break;
      default:
      printf("étrange...\n");
      break;
      ;
    }

  } while(saisie[0] != 's');

  return(0);
}
