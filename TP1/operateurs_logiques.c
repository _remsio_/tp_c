#include <stdio.h>
#include <string.h>

int main() {

  int X;


  X = 5 & 6;
  printf("opération 1 [X = 5 & 6 = %d] \n", X);
  printf("Cette opération est un ET bit à bit 5 : 0101, 6 : 0110 --> 0100 soit 4 \n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = 5 && 6;
  printf("opération 2 [X = 5 && 6 = %d] \n", X);
  printf("Cette opération est un ET logique, elle renvoie 1 si les 5 et 6 sont vrais, sinon 0 (pas de type boléen en c), or les nombres > 0 sont considérés comme vrais donc 1\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = 5 | 6;
  printf("opération 3 [X = 5 | 6 = %d] \n", X);
  printf("Cette opération est un OU bit à bit 5 : 0101, 6 : 0110 --> 0111 soit 7\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = 5 || 6;
  printf("opération 4 [X = 5 || 6 = %d] \n", X);
  printf("Cette opération est un OU logique, elle renvoie 1 si les 5 ou 6 est vrai, sinon 0 (pas de type boléen en c), or un des nombres > 0 sont considérés comme vrais donc 1\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = 5 ^ 6;
  printf("opération 5 [X = 5 ^ 6 = %d] \n", X);
  printf("Cette opération est un XOR bit à bit 5 : 0101, 6 : 0110 --> 0011 soit 3\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = ~5;
  printf("opération 6 [~5 = %d] \n", X);
  printf("Cette opération est une négation binaire, elle renvoie la valeur négative d'un nombre \n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

  X = !5;
  printf("opération 7 [!5 = %d] \n", X);
  printf("Cette opération est une négation booléenne, elle renvoie l'inverse du résultat d'une opération logique, un nombre positif étant considéré comme vrai, le résultat est ici 0 \n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  return 0;
}
