#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
* Fonction prenant une chaine de caractères en paramètres et retournant le
* nombre de voyelles qu'elle contient.
*
*/
int nombreVoyelle(char chaine[])
{
  char voyelles[6] = {'a', 'e', 'i', 'o', 'u', 'y'};

  int result = 0;

  for(int i=0; i < strlen(chaine); i++){
      for(int j=0; j < strlen(voyelles); j++){
        if(chaine[i] == voyelles[j]){
          result ++;
        }
      }
  }

  return result;
}

int main(int argc, char *argv[]) {

  char saisie[50] = "";
  printf("chaine de caractères finissant par # : ");
  fgets(saisie, 50, stdin);
  if(saisie[strlen(saisie)-2] != '#'){
    fprintf(stderr, "%s", "Le dernier caractère doit être un #!\n");
    return(1);
  }
  // On retire le \n ajouté automatiquement par fgets
  saisie[strlen(saisie)-1] = '\0';

  printf("La chaine [%s] contient %d voyelles\n", saisie, nombreVoyelle(saisie));

  return(0);
}
