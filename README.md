
#makefile

## générer tous les exécutables

`make all`

## générer un seul exécutable

`make <nom_fichier_sans_.c>`

## générer tous les exécutables d'un dossier

`make <nom_dossier>`

## vider le dossier d'exécutables

`make clean`
