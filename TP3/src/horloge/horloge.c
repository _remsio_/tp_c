#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "horloge.h"



int main() {

  int heures;
  int minutes;
  int secondes;
  char saisie[5] = "";

  do {
    printf("Nombre d'heures : ");
    fgets(saisie, 5, stdin);
    heures = atoi(saisie);
  } while(heures > NB_HEURES_PAR_JOUR || heures < 0);

  do {
    printf("Nombre de minutes : ");
    fgets(saisie, 5, stdin);
    minutes = atoi(saisie);
  } while(minutes > NB_MINUTES_PAR_HEURE || minutes < 0);

  do {
      printf("Nombre de secondes : ");
      fgets(saisie, 5, stdin);
      secondes = atoi(saisie);
  } while(secondes > NB_SECONDES_PAR_MINUTE || secondes < 0);

  printf("%d heure(s) %d minute(s) %d seconde(s) est équivalent à %d seconde(s)\n", heures, minutes, secondes, convertTime(heures, minutes, secondes));

  return 0;
}
