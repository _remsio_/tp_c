int NB_SECONDES_HEURE = 3600;
int NB_SECONDES_MINUTE = 60;
int NB_HEURES_PAR_JOUR = 24;
int NB_MINUTES_PAR_HEURE = 60;
int NB_SECONDES_PAR_MINUTE = 60;

int convertTime(int heure, int minutes, int secondes)
{

  int result = 0;

  result += heure * NB_SECONDES_HEURE;
  result += minutes * NB_SECONDES_MINUTE;
  result += secondes;
  return result;

}
