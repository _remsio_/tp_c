#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "degre.c"


int main() {

  printf("Conversion de Celsius à Kelvin CelsiusAKelvin(15.0) : ");
  printf("%.1f \n", CelsiusAKelvin(15.0));
  printf("Conversion de Celsius à Fahrenheit CelsiusAFahrenheit(15.0) : ");
  printf("%.1f \n", CelsiusAFahrenheit(15.0));
  printf("Conversion de Kelvin à Celsius KelvinACelsius(288.0) : ");
  printf("%.1f \n", KelvinACelsius(288.0));
  printf("Conversion de Fahrenheit à Celsius FahrenheitACelsius(40.3) : ");
  printf("%.1f \n", FahrenheitACelsius(40.3));
  printf("Conversion de Fahrenheit à Kelvin FahrenheitAKelvin(40.3) : ");
  printf("%.1f \n", FahrenheitAKelvin(40.3));
  printf("Conversion de Kelvin à Fahrenheit KelvinAFahrenheit(288.0) : ");
  printf("%.1f \n", KelvinAFahrenheit(288.0));

  return(0);

}
