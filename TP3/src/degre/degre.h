float CONSTANTE_KELVIN = 273.0;
float CONSTANTE_FAHRENHEIT = 32.0;
float CONSTANTE_FLOTTANTE_FAHRENHEIT = 5.0 / 9.0;


float CelsiusAKelvin(float celsius);
float CelsiusAFahrenheit(float celsius);
float KelvinACelsius(float kelvin);
float FahrenheitACelsius(float fahrenheit);
float FahrenheitAKelvin(float fahrenheit);
float KelvinAFahrenheit(float kelvin);
