#include "degre.h"

float CelsiusAKelvin(float celsius)
{
  float kelvin = celsius + CONSTANTE_KELVIN;
  return kelvin;
}


float CelsiusAFahrenheit(float celsius)
{

  float fahrenheit = CONSTANTE_FLOTTANTE_FAHRENHEIT * celsius + CONSTANTE_FAHRENHEIT;
  return fahrenheit;

}

float KelvinACelsius(float kelvin)
{
  float celsius = kelvin - CONSTANTE_KELVIN;
  return celsius;
}

float FahrenheitACelsius(float fahrenheit)
{
  float celsius = (fahrenheit - CONSTANTE_FAHRENHEIT) / CONSTANTE_FLOTTANTE_FAHRENHEIT;
  return celsius;
}


float FahrenheitAKelvin(float fahrenheit)
{
  float kelvin = CelsiusAKelvin(FahrenheitACelsius(fahrenheit));
  return kelvin;
}

float KelvinAFahrenheit(float kelvin)
{
  float fahrenheit = CelsiusAFahrenheit(KelvinACelsius(kelvin));
  return fahrenheit;
}
