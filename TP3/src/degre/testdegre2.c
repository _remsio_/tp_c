#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "degre.c"


int lireChoix() {

  int choix_menu = 0;
  char saisie[5] = "";

  do {

    if(choix_menu < 0 || choix_menu > 6)
    {
      printf("Le choix doit être compris entre 0 et 6\n");
    }

    printf("Votre choix : ");
    fgets(saisie, 5, stdin);
    choix_menu = atoi(saisie);
  } while(choix_menu < 0 || choix_menu > 6);

  return choix_menu;
}

void executerChoix(int choix) {

  float valeur = 0.0;
  char saisie[15] = "";

  do {
    if(valeur < -2000.0 || valeur > 2000.0)
    {
      printf("La valeur doit être comprise entre -2000.0 et 2000.0\n");
    }

    printf("saisir la valeur : ");
    fgets(saisie, 15, stdin);
    valeur = atof(saisie);
  } while(valeur < -2000.0  || valeur > 2000.0);

  switch (choix) {
    case 1:
    printf("\nconversion de %.1f degré Celsius en Kelvin : %.1f\n\n", valeur, CelsiusAKelvin(valeur));
    break;
    case 2:
    printf("\nconversion de %.1f degré Celsius en Fahrenheit : %.1f\n\n", valeur, CelsiusAFahrenheit(valeur));
    break;
    case 3:
    printf("\nconversion de %.1f degré Kelvin en Celsius : %.1f\n\n", valeur, KelvinACelsius(valeur));
    break;
    case 4:
    printf("\nconversion de %.1f degré Fahrenheit en Celsius : %.1f\n\n", valeur, FahrenheitACelsius(valeur));
    break;
    case 5:
    printf("\nconversion de %.1f degré Fahrenheit en Kelvin : %.1f\n\n", valeur, FahrenheitAKelvin(valeur));
    break;
    case 6:
    printf("\nconversion de %.1f degré Kelvin en Fahrenheit : %.1f\n\n", valeur, KelvinAFahrenheit(valeur));
    break;
    default:
    printf("\nEtrange...\n\n");
    break;
  }
}

/*
* Procedure affichant les options du menu
*
*/
void menu()
{
  printf("Ce programme permet d'effectuer des conversions de degré à celsius, kelvin ou fahrenheit \n");
  printf("Choix disponibles :\n");
  printf("0) Quitter\n");
  printf("1) Celsius à Kelvin\n");
  printf("2) Celsius à Fahrenheit\n");
  printf("3) Kelvin à Celsius\n");
  printf("4) Fahrenheit à Celsius\n");
  printf("5) Fahrenheit à Kelvin\n");
  printf("6) Kelvin à Fahrenheit\n\n");
}

int main() {

  int choix_menu = -1;

  do {

    menu();
    choix_menu = lireChoix();
    if(choix_menu == 0)
    {
      break;
    }
    executerChoix(choix_menu);

  } while(choix_menu != 0);

  return(0);

}
