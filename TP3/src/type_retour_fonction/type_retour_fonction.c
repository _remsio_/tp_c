#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "type_retour_fonction.h"

int compareIntReturnInt(int int1, int int2)
{
  if(int1 == int2)
  {
    return 1;
  }
  return 0;
}

char compareIntReturnChar(int int1, int int2)
{
  if(int1 == int2)
  {
    return 'V';
  }
  return 'F';
}

int compareFloatReturnInt(float float1, float float2)
{
  if(float1 == float2)
  {
    return 1;
  }
  return 0;
}

char compareFloatReturnChar(float float1, float float2)
{
  if(float1 == float2)
  {
    return 'V';
  }
  return 'F';
}

int main() {

  printf("Comparaison de 2 entiers égaux, réponse en entier compareIntReturnInt(15,15) : ");
  printf("%d \n", compareIntReturnInt(15,15));
  printf("Comparaison de 2 entiers non égaux, réponse en entier compareIntReturnInt(15,14) : ");
  printf("%d \n", compareIntReturnInt(15,14));
  printf("Comparaison de 2 entiers égaux, réponse avec un caractère compareIntReturnChar(99,99) : ");
  printf("%c \n", compareIntReturnChar(99,99));
  printf("Comparaison de 2 entiers non égaux, réponse avec un caractère compareIntReturnChar(23,17) : ");
  printf("%c \n", compareIntReturnChar(23,17));
  printf("Comparaison de 2 flottants égaux, réponse en entier compareFloatReturnInt(15.0,15.0) : ");
  printf("%d \n", compareFloatReturnInt(15.0,15.0));
  printf("Comparaison de 2 flottants non égaux, réponse en entier compareFloatReturnInt(17.2,67.1) : ");
  printf("%d \n", compareFloatReturnInt(17.2,67.1));
  printf("Comparaison de 2 flottants égaux, réponse avec un caractère compareFloatReturnChar(15.0,15.0) : ");
  printf("%c \n", compareFloatReturnChar(15.0,15.0));
  printf("Comparaison de 2 flottants non égaux, réponse avec un caractère compareFloatReturnChar(17.2,67.1) : ");
  printf("%c \n", compareFloatReturnChar(17.2,67.1));
  return 0;
}
