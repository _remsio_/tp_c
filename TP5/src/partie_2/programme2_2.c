#include <stdio.h>
void main()
{
 int i,j,k,tmp; // création des entiers i,j,k et tmp
 int *pi,*pj,*pk,*ptmp; // création des pointeurs d'entier pi,pj,pk et ptmp
i=31 ; j=52 ; k=17; // initialisation des valeurs de i à 31, j à 52 et k à 17
 pi=&i ; pj=&j ; pk=&k; // définition des adresses pointées par pi sur l'adresse i,
                        // pj sur l'adresse j,et pk sur l'adresse de k
 printf("i=%d j=%d k=%d\n",i,j,k); // affichage des valeurs de i, j et k
 printf("*pi=%d *pj=%d *pk=%d\n",*pi,*pj,*pk); // affichage des valeurs contenues
                                              // dans les adresses pointées par pi, pj et pk
 tmp=*pi; // tmp est initialisé sur le contenu pointé par pi
 *pi=*pj; // le contenu pointé par pi devient le contenu pointé par pj
 *pj=*pk; // le contenu pointé par pj devient le contenu pointé par pk
 *pk=tmp; // le contenu pointé par pk devient le contenu de tmp
 printf("i=%d j=%d k=%d\n",i,j,k);  // affichage des valeurs de i, j, k
 printf("*pi=%d *pj=%d *pk=%d\n",*pi,*pj,*pk); // affichage des valeurs pointées par pi, pj et pk
 ptmp=pi; // l'adresse pointée par ptmp devient l'adresse pointée par pi
 pi=pj; // l'adresse pointée par pi devient l'adresse pointée par pj
 pj=pk; // l'adresse pointée par pj devient l'adresse pointée par pk
 pk=ptmp; // l'adresse pointée par pk devient l'adresse pointée par ptmp
 printf("i=%d j=%d k=%d\n",i,j,k); // affichage des valeurs de i,j et k
 printf("*pi=%d *pj=%d *pk=%d\n",*pi,*pj,*pk); // affichage des valeurs pointées par pi,pj et pk
}
