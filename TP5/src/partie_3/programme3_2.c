/*
* TP 5 : Pointeurs
*
* insuffisance des appels de fonction
* avec passage par valeur
*/
#include <stdio.h>
void echanger(int *pa, int *pb)
{
 int tmp; // définition de l'entier tmp

 tmp = *pa; // l'entier tmp prend la valeur de a
 *pa = *pb; // l'entier a prend la valeur de b
 *pb = tmp; // l'entier b prend la valeur de tmp
}
void main()
{
 int x,y; // définition de l'entier x et y

 x = 12; // initialisation de l'entier x à 12
 y = 34; // initialisation de l'entier y à 34

 printf("Avant échange : x = %d ; y = %d\n",x,y);

 echanger(&x,&y);

 printf("Après échange : x = %d ; y = %d\n",x,y);
}
