#include <stdio.h>
#include <stdlib.h>

void echangerPointeurs(int **ppa, int **ppb)
{
 int *tmp;

 tmp = *ppa; // adresse du pointeur tmp = adresse pointée
             // par le pointeur par ppa
 *ppa = *ppb; // adresse pointée par le pointeur par ppa
             // = adresse pointée par le pointeur par ppb
 *ppb = tmp; // adresse pointée par le pointeur par ppb
             // = adresse du pointeur tmp
}

void main()
{
 int *px,*py; // définition de l'entier x et y

 px = (int *) malloc (sizeof(int));
 py = (int *) malloc (sizeof(int));

 *px = 12; // initialisation de l'entier pointé par px à 12
 *py = 34; // initialisation de l'entier pointé par py à 34

 printf("Avant échange : *px = %d ; *py = %d ; px = %u ; py = %u \n",*px,*py, px,py);

 echangerPointeurs(&px,&py);

 printf("Après échange : *px = %d ; *py = %d ; px = %u ; py = %u \n",*px,*py, px,py);
}
