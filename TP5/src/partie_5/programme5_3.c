#include <stdio.h>
#include <stdlib.h>

#define N 9

typedef struct maillon {
int x;
struct maillon * suiv;
} maillon;

/*
* Fonction de suppression de maillon
*/
maillon* supprimerMaillon(maillon *tete, int index){
  maillon* precedent;
  maillon* tmp;

  //Si le premier element correspond
  if (index == tete->x) {
    precedent = tete;
    tete = tete->suiv;
    free(precedent);
    return tete;
  }

  precedent = NULL;
  tmp = tete;
  // on boucle jusqu'à rencontrer l'index
  while (tmp->suiv != NULL && index != tmp->x) {
    precedent = tmp;
    tmp = tmp->suiv;
  }

  // Si l'element existe on le supprime, sinon on affiche un message d'erreur
  if (tmp != NULL && index == tmp->x) {
    precedent->suiv = tmp->suiv;
    free(tmp);
  } else {
    printf("Le numero de maillon n'existe pas\n");
  }

  return tete;

}

void main(void)
{
  maillon *lc;
  maillon *tete;
  int cpt;
  char saisie[20] = "";
  int index = 0;
  /*init des maillons*/
  lc = (maillon *) malloc(sizeof(maillon));
  tete = lc;
  //Creation des maillons en fin de liste
  for(cpt=1;cpt<N;cpt++) //pour tous les maillons à créer
  {
    lc->suiv = (maillon *) malloc(sizeof(maillon));
    lc = lc->suiv;
  }
  lc->suiv = NULL;
  cpt = 0;
  lc = tete;
  //remplissage des valeurs de chacun des maillons
  while (lc != NULL) //tant que le maillon courant n'est pas le suivant du dernier maillon de la liste
  {
    lc->x = cpt; //affectation
    cpt++; //increment du compteur
    lc = lc->suiv;//passe au suivant
  }
  lc = tete;

  do {
    if(index < 0 || index > N-1){
      printf(" L'index est compris entre %d et %d \n",0 ,N-1 );
    }

    printf("index : ");
    fgets(saisie, 10, stdin);
    index = atoi(saisie);
  } while(index > N-1 || index < 0);

  lc = supprimerMaillon(lc, index);

  while (lc != NULL) //tant que le maillon courant n'est pas le suivant du dernier maillon de la liste
  {
    printf("Valeur du champs courant = %d\n",lc->x);
    printf("Adresse maillon courant= %X et du suivant %X\n",lc, lc->suiv);
    lc = lc->suiv;//passe au suivant
  }
}
