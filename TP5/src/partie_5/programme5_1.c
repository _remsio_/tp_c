#include <stdio.h>
#include <stdlib.h>

#define N 3

typedef struct maillon {
int x;
struct maillon * suiv;
} maillon;

void main(void)
{
  maillon lc; // création d'un maillon
  lc.x = 1; // valeur x du maillon initialisée à 1
  printf("Valeur du champs x = %d\n\n", lc.x);
  lc.suiv = (maillon *) malloc(sizeof(maillon)); // creation d'une case memoire de la taille d'un maillon
  lc.suiv->x = 2; // valeur x de la structure pointée par lc.suiv initialisée à 2
  printf("Valeur du champs x du deuxieme maillon = %d\n\n", lc.suiv->x);
}
