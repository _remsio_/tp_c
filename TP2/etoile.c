#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
* Procedure affichant les options du menu
*
*/
void menu()
{
  printf("Ce programme permet d'afficher divers figures geometriques d'une taille n a definir\n");
  printf("Choix disponibles :\n");
  printf("1) Dessiner carre\n");
  printf("2) Dessiner une fleche vers la droite\n");
  printf("3) Dessiner une fleche vers la gauche\n");
  printf("4) Dessiner un triangle\n");
  printf("5) Dessiner un losange\n");
  printf("6) Quitter le menu\n\n");
}


/*
* Procedure prenant une taille n en entrée et dessinant un carré de cette taille
*
*/
void dessinerCarre(int n)
{

  for(int i = 0; i < n; i++){

    for(int j = 0; j<n; j++){
      printf("*");
    }
    printf("\n");

  }
}

/*
* Procedure prenant une taille n en entrée et dessinant une flèche vers la droite de cette taille
*
*/
void dessinerFlecheDroite(int n)
{

  // Dessus de la flèche
  for(int i = 0; i < n; i++){

    for(int j = 0; j<i; j++){
      printf("*");
    }
    printf("\n");
  }
  //Dessous de la flèche
  for(int i = 0; i < n; i++){

    for(int j = 0; j<n-i; j++){
      printf("*");
    }
    printf("\n");

  }
}

/*
* Procedure prenant une taille n en entrée et dessinant une flèche vers la gauche de cette taille
*
*/
void dessinerFlecheGauche(int n)
{

  // Dessus de la flèche
  for(int i = 0; i < n; i++){
    for(int j = 0; j<n -i; j++){
      printf(" ");
    }
    for(int j = 0; j<i; j++){
      printf("*");
    }
    printf("\n");
  }
  //Dessous de la flèche
  for(int i = 0; i < n; i++){
    for(int j = 0; j<i; j++){
      printf(" ");
    }
    for(int j = 0; j<n-i; j++){
      printf("*");
    }
    printf("\n");

  }
}

/*
* Procedure prenant une taille n en entrée et dessinant un triangle de cette taille
*
*/
void dessinerTriangle(int n)
{

  // coté gauche du triangle
  for(int i = 0; i < n; i++){
    for(int j = 0; j<n -i; j++){
      printf(" ");
    }
    for(int j = 0; j<i; j++){
      printf("*");
    }
    printf("*");
    for(int j = 0; j<i; j++){
      printf("*");
    }
    for(int j = 0; j<n-i; j++){
      printf(" ");
    }
    printf("\n");

  }
}

/*
* Procedure prenant une taille n en entrée et dessinant un triangle inverse de cette taille
*
*/
void dessinerTriangleInverse(int n)
{

  // coté gauche du triangle
  for(int i = 0; i < n; i++){

    for(int j = i+2; j>0; j--){
      printf(" ");
    }
    for(int j = n-i; j>1; j--){
      printf("*");
    }

    printf("*");

    for(int j = n-i; j>1; j--){
      printf("*");
    }

    for(int j = i; j>0; j--){
      printf(" ");
    }

    printf("\n");

  }
}

/*
* Procedure prenant une taille n en entrée et dessinant un losange de cette taille
*
*/
void dessinerLosange(int n)
{
  dessinerTriangle(n);
  dessinerTriangleInverse(n-1);
}

int main(int argc, char *argv[]) {

  int choix_menu = 0;
  int n = 0;
  char saisie[5] = "";

  do {
    menu();
    do {

      if(choix_menu < 1 || choix_menu > 6)
      {
        printf("Le choix doit être compris entre 1 et 6\n");
      }

      printf("Votre choix : ");
      fgets(saisie, 5, stdin);
      choix_menu = atoi(saisie);
    } while(choix_menu < 1 || choix_menu > 6);

    if(choix_menu == 6)
    {
      printf("Au revoir\n");
      break;
    }

    do {

      if(n < 1 || n > 20)
      {
        printf("n doit être compris entre 1 et 20\n");
      }

      printf("saisir n : ");
      fgets(saisie, 5, stdin);
      n = atoi(saisie);
    } while(n < 1 || n > 20);

    switch (choix_menu) {
      case 1:
      printf("\nCarré : \n");
      dessinerCarre(n);
      break;
      case 2:
      printf("\nFlèche à droite : \n");
      dessinerFlecheDroite(n);
      break;
      case 3:
      printf("\nFlèche à gauche : \n");
      dessinerFlecheGauche(n);
      break;
      case 4:
      printf("\nTriangle : \n");
      dessinerTriangle(n);
      break;
      case 5:
      printf("\nLosange : \n");
      dessinerLosange(n);
      break;
      default:
      printf("étrange...\n");
      break;
    }

  } while(choix_menu != 6);

  return(0);
}
