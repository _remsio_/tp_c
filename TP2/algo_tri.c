#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

void triBulles(int *tab, int tailleTab)
{

  int tmp;
  int continuerPermutation = 1;
  while(continuerPermutation != 0)
  {
    continuerPermutation = 0;
    for (int i = 0 ; i < tailleTab -1 ; i++)
      {
          if (tab[i] > tab[i+1])
          {
            tmp = tab[i];
            tab[i] = tab[i+1];
            tab[i+1] = tmp;
            continuerPermutation = 1;
          }
      }
  }

}

void triRechercheMinima(int *tab, int tailleTab)
{

  int tmpIteration = 0;
  int tmpValeur = tab[0];
  int tmpValeur2 = tab[0];
  for (int i = 0 ; i < tailleTab; i++)
  {

    tmpIteration = i;
    tmpValeur = tab[i];
    tmpValeur2 = tab[i];
    for (int j = i+1; j < tailleTab; j++)
    {
      if (tmpValeur > tab[j])
      {
        tmpIteration = j;
        tmpValeur = tab[j];
      }
    }
    tmpValeur2 = tab[i];
    tab[i] = tmpValeur;
    tab[tmpIteration] = tmpValeur2;
  }

}

void triInsertion(int *tab, int tailleTab)
{
  int j = 0;
  int i;

   for ( i = 1; i < tailleTab; i++) {
       int elem = tab[i];
       j = i;
       while(j > 0 && tab[j-1] > elem)
       {
         tab[j] = tab[j-1];
         j --;
       }

       tab[j] = elem;
       j = 0;
   }



}

int main() {

  int tableau[30];

  int tailleTableau = sizeof(tableau)/sizeof(int);
  for(int i = 0; i < tailleTableau; i++)
  {
    tableau[i] = rand()%50;
  }
  triBulles(tableau, tailleTableau);
  printf("Tableau 1 trié\n");
  printf("[");
  for(int i = 0; i < tailleTableau; i++)
  {
    printf("%d ",tableau[i]);
  }
  printf("]\n");

  for(int i = 0; i < tailleTableau; i++)
  {
    tableau[i] = rand()%50;
  }

  triRechercheMinima(tableau, tailleTableau);
  printf("Tableau 2 trié\n");
  printf("[");
  for(int i = 0; i < tailleTableau; i++)
  {
    printf("%d ",tableau[i]);
  }
  printf("]\n");

  for(int i = 0; i < tailleTableau; i++)
  {
    tableau[i] = rand()%50;
  }

  triInsertion(tableau, tailleTableau);

  printf("Tableau 3 trié\n");
  printf("[");
  for(int i = 0; i < tailleTableau; i++)
  {
    printf("%d ",tableau[i]);
  }
  printf("]\n");

  return 0;
}
