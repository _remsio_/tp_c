#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "carnet_adresse.h"


/*
* Procedure affichant les options du menu
*
*/
void menu()
{
  printf("\n\nCe programme permet de gérer un groupe de personnes\n");
  printf("Choix disponibles :\n");
  printf("1) Ajouter personne\n");
  printf("2) Afficher informations déjà saisies\n");
  printf("3) Trier par date de naissance\n");
  printf("4) Modifier personnes par rapport au nom\n");
  printf("5) Supprimer enregistrement\n");
  printf("6) Quitter le menu\n\n");
}

/*
* Fonction retournant un choix entre 0 et 6 saisie par l'utilisateur
*/
int lireChoix() {

  int choix_menu = 0;
  char saisie[5] = "";

  do {

    if(choix_menu < 0 || choix_menu > 6)
    {
      printf("Le choix doit être compris entre 0 et 6\n");
    }

    printf("Votre choix : ");
    fgets(saisie, 5, stdin);
    choix_menu = atoi(saisie);
  } while(choix_menu < 0 || choix_menu > 6);

  return choix_menu;
}

/*
* Permet d'intéragir avec les différentes fonctions de gestion de personnes
*/
void executerChoix(int choix, personne tabPersonne[], int tailleTab) {

  char saisie[20] = "";
  char nomModif[20] = "";

  switch (choix) {
    case 1:
      if(tailleTab == NB_PERSONNE_MAX){
        printf("Nombre de personne max atteinte : %d \n", NB_PERSONNE_MAX );
        break;
      }

      printf("\nSaisissez les informations propre à la personne\n\n");
      personne personneAdd = definirPersonne();
      tabPersonne[tailleTab] = personneAdd;
      break;
    case 2:
      printf("\nAffichage des informations déjà saisies...\n\n");
      afficherPersonnes(tabPersonne, tailleTab);
      break;
    case 3:
      printf("\nTri par date de naissance...\n\n");
      classerPersonnes(tabPersonne, tailleTab);
      break;
    case 4:

      printf("Nom de la ou des personnes à modifier : ");
      fgets(saisie, 20, stdin);
      saisie[strlen(saisie)-1] = '\0';
      strcpy( nomModif, saisie );

      printf("\nChoisissez le nom de la personne à modifier\n\n");
      setPersonnes(tabPersonne, tailleTab, nomModif);
      break;
    case 5:
      printf("\nSelectionnez le numero de la personne à supprimer\n\n");
      int position;
      do {
        printf("position (0-%d) : ", tailleTab-1);
        fgets(saisie, 10, stdin);
        position = atoi(saisie);
      } while(position > NB_PERSONNE_MAX || position < 0);

      supprimerPersonne(position, tabPersonne, tailleTab);

      break;
    case 6:
      printf("\nAu revoir\n\n");
      break;
    default:
      printf("\nEtrange...\n\n");
      break;
  }
}

/*
  retourne 1 si l'annee est bissexile sinon 0
*/
int isBissexile(int annee)
{

  if(annee % 4 == 0 && annee % 100 != 0)
  {
    return 1;
  }

  if(annee % 400 == 0)
  {
    return 1;
  }
  return 0;
}


/*
* retourne une personne créée par l'utilisateur
*/
personne definirPersonne()
{
  personne personne_temp;
  char saisie[20] = "";
  char nom[20] = "";
  char prenom[20] = "";
  int jour = JOUR_MIN;
  int mois = MOIS_MIN;
  int annee = ANNEE_MIN;
  personne personne1;


  printf("Nom de l'utilisateur : ");
  fgets(saisie, 20, stdin);
  saisie[strlen(saisie)-1] = '\0';
  strcpy( nom, saisie );

  printf("Prenom de l'utilisateur : ");
  fgets(saisie, 20, stdin);
  saisie[strlen(saisie)-1] = '\0';
  strcpy( prenom, saisie );

  printf("Vous allez maintenant saisir la date de naissance de l'utilisateur\n ");
  printf("l'année, le mois puis le jour de naissance seront renseignés\n ");

  do {
    if(annee < ANNEE_MIN || annee > ANNEE_MAX){
      printf("L'annee doit être comprise entre %d et %d \n",ANNEE_MIN ,ANNEE_MAX );
    }

    printf("Annee de naissance : ");
    fgets(saisie, 10, stdin);
    annee = atoi(saisie);
  } while(annee > ANNEE_MAX || annee < ANNEE_MIN);

  do {

    if(mois < MOIS_MIN || mois > NB_MOIS_PAR_AN){
      printf("Le mois doit être compris entre %d et %d \n",MOIS_MIN ,NB_MOIS_PAR_AN );
    }

    printf("Mois de naissance : ");
    fgets(saisie, 10, stdin);
    mois = atoi(saisie);
  } while(mois > NB_MOIS_PAR_AN || mois < MOIS_MIN);

  int nb_jours_max = 0;
  switch (mois) {
    case 2:
      if (isBissexile(annee)){
          nb_jours_max = 29;
      } else {
        nb_jours_max = 28;
      }
      break;
    case 4:
      nb_jours_max = 30;
      break;
    case 6:
      nb_jours_max = 30;
      break;
    case 9:
      nb_jours_max = 30;
      break;
    case 11:
      nb_jours_max = 30;
      break;
    default:
      nb_jours_max = 31;
      break;
  }


  do {

    if(mois < nb_jours_max || mois > JOUR_MIN){
      printf("Le jour doit être compris entre %d et %d \n",JOUR_MIN ,nb_jours_max );
    }

    printf("Jour de naissance : ");
    fgets(saisie, 10, stdin);
    jour = atoi(saisie);
  } while(jour > nb_jours_max || jour < JOUR_MIN);

  strcpy( personne1.nom, nom );
  strcpy( personne1.prenom, prenom );
  personne1.date.jour = jour;
  personne1.date.mois = mois;
  personne1.date.annee = annee;

  return personne1;
}

void setPersonnes(personne *tabPersonnes, int tailleTab, char nom[]){
  personne tabPersonne[20];
  int nbPersonnes = 0;
  personne tmp_personne;

  for(int i = 0; i < tailleTab; i++ ){

    if(strcmp(tabPersonnes[i].nom, nom) == 0){
      afficherPersonne(tabPersonnes, i);
      tabPersonne[nbPersonnes] = tabPersonnes[i];
      tmp_personne = definirPersonne();
      tabPersonnes[i] = tmp_personne;
      nbPersonnes ++;
    }
  }

  printf("Les personnes suivantes ont été modifiées\n" );
  afficherPersonnes(tabPersonne, nbPersonnes);

}

void afficherPersonnes(personne *tabPersonnes, int tailleTab){

  for(int i = 0; i < tailleTab; i++ ){
    printf("[id:%d, nom:%s, prenom:%s, né le:%d/%d/%d]\n", i, tabPersonnes[i].nom, tabPersonnes[i].prenom, tabPersonnes[i].date.jour, tabPersonnes[i].date.mois, tabPersonnes[i].date.annee);
  }

}

void afficherPersonne(personne *tabPersonnes, int position){
  printf("[nom:%s prenom:%s né le date:%d/%d/%d]\n",tabPersonnes[position].nom, tabPersonnes[position].prenom, tabPersonnes[position].date.jour, tabPersonnes[position].date.mois, tabPersonnes[position].date.annee);
}

void classerPersonnes(personne *tab, int tailleTab){
  personne tmp;
  int continuerPermutation = 1;
  while(continuerPermutation != 0)
  {
    continuerPermutation = 0;
    for (int i = 0 ; i < tailleTab -1 ; i++)
    {
      if (tab[i].date.annee > tab[i+1].date.annee)
      {
        tmp = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = tmp;
        continuerPermutation = 1;
      }
      if (tab[i].date.annee == tab[i+1].date.annee)
      {
        if (tab[i].date.mois > tab[i+1].date.mois)
        {
          tmp = tab[i];
          tab[i] = tab[i+1];
          tab[i+1] = tmp;
          continuerPermutation = 1;
        }
        if (tab[i].date.mois == tab[i+1].date.mois)
        {
          if (tab[i].date.jour > tab[i+1].date.jour)
          {
            tmp = tab[i];
            tab[i] = tab[i+1];
            tab[i+1] = tmp;
            continuerPermutation = 1;
          }
        }
      }
    }
  }
}

/*
* Fonction déplaçant toutes les personnes
*/
void supprimerPersonne(int position, personne *tabPersonnes, int tailleTab){
    for(int i; i < tailleTab; i++){
      tabPersonnes[i] = tabPersonnes[i+1];
    }
}

int main() {

  personne tabPersonne[NB_PERSONNE_MAX];

  int choix_menu = -1;
  int nbPersonnes = 0;

  do {

    menu();
    choix_menu = lireChoix();
    if(choix_menu == 0)
    {
      break;
    }
    executerChoix(choix_menu, tabPersonne, nbPersonnes);
    if(choix_menu == 1){
      nbPersonnes ++;
    }

    if(choix_menu == 5){
      nbPersonnes --;
    }

  } while(choix_menu != 0);

  return 0;
}
