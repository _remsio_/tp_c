int NB_MOIS_PAR_AN = 12;
int ANNEE_MAX = 3000;
int ANNEE_MIN = -3000;
int JOUR_MIN = 1;
int MOIS_MIN = 1;
int NB_PERSONNE_MAX = 20;

typedef struct{
  int jour;
  int mois;
  int annee;
} date;

typedef struct{
  char nom[20];
  char prenom[20];
  date date;
} personne;

void afficherPersonnes(personne *tabPersonnes, int tailleTab);
void afficherPersonne(personne *tabPersonnes, int position);
int isBissexile(int annee);
personne definirPersonne();
void setPersonnes(personne *tabPersonnes, int tailleTab, char nom[]);
void classerPersonnes(personne *tab, int tailleTab);
void supprimerPersonne(int position, personne *tabPersonnes, int tailleTab);
