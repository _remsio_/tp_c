#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "controle_acces.h"

/*
* Procedure affichant les options du menu
*
*/
void menu()
{
  printf("\n\nCe programme permet de gérer une liste de cartes à controle d'accès\n");
  printf("Choix disponibles :\n");
  printf("1) Afficher la liste des personnes enregistrées\n");
  printf("2) Ajouter une personne à la liste\n");
  printf("3) Supprimer une personne selon le numéro de badge\n");
  printf("4) Modifier le code secret d'une personne\n");
  printf("5) Simuller le controle d'accès\n");
  printf("6) Sauvegarder la liste de personnes dans un fichier\n");
  printf("7) Charger fichier de personnes\n");
  printf("0) Quitter le menu\n\n");
}

/*
* Fonction retournant un choix entre 0 et 8 saisie par l'utilisateur
*/
int lireChoix() {

  int choix_menu = 0;
  char saisie[5] = "";

  do {

    if(choix_menu < 0 || choix_menu > 8)
    {
      printf("Le choix doit être compris entre 0 et 8\n");
    }

    printf("Votre choix : ");
    fgets(saisie, 5, stdin);
    choix_menu = atoi(saisie);
  } while(choix_menu < 0 || choix_menu > 8);

  return choix_menu;
}

/*
* Permet d'intéragir avec les différentes fonctions de gestion de personnes
*/
struct maillon* executerChoix(int choix, struct maillon *liste) {

  char saisie[20] = "";
  int numeroBadge = 0;
  char codeSecret[100] = "";

  switch (choix) {
    case 0:
      printf("\nAu revoir\n\n");
      break;
    case 1:
      afficherTousMaillons(liste);
      break;
    case 2:
      liste = ajouterMaillon(liste);
      break;
    case 3:
      do {
        if(numeroBadge < MIN_NUMERO_BADGE || numeroBadge > MAX_NUMERO_BADGE){
          printf(" Le code doit faire 4 chiffres %d et %d \n",MIN_NUMERO_BADGE ,MAX_NUMERO_BADGE );
        }
        printf("Numéro de badge : ");
        fgets(saisie, 10, stdin);
        numeroBadge = atoi(saisie);
      } while(numeroBadge > MAX_NUMERO_BADGE || numeroBadge < MIN_NUMERO_BADGE);
      liste = supprimerPersonne(liste, numeroBadge);

      break;
    case 4:
      do {
        if(numeroBadge < MIN_NUMERO_BADGE || numeroBadge > MAX_NUMERO_BADGE){
          printf(" Le code doit faire 4 chiffres %d et %d \n",MIN_NUMERO_BADGE ,MAX_NUMERO_BADGE );
        }
        printf("Numéro de badge : ");
        fgets(saisie, 10, stdin);
        numeroBadge = atoi(saisie);
      } while(numeroBadge > MAX_NUMERO_BADGE || numeroBadge < MIN_NUMERO_BADGE);
      liste = modifierCodeSecret(liste, numeroBadge);
      break;
    case 5:

      do {
        if(numeroBadge < MIN_NUMERO_BADGE || numeroBadge > MAX_NUMERO_BADGE){
          printf(" Le code doit faire 4 chiffres %d et %d \n",MIN_NUMERO_BADGE ,MAX_NUMERO_BADGE );
        }
        printf("Saisissez votre numero de badge : ");
        fgets(saisie, 10, stdin);
        numeroBadge = atoi(saisie);
      } while(numeroBadge > MAX_NUMERO_BADGE || numeroBadge < MIN_NUMERO_BADGE);

      printf("Saisissez votre code secret : ");
      fgets(codeSecret, 100, stdin);
      codeSecret[strlen(codeSecret)-1] = '\0';
      liste = simulerControle(liste, codeSecret, numeroBadge);


      break;
    case 6:
      enregistrerListeDansFichier(liste);
      break;
    case 7:
      liste = chargerListeDansFichier();
      break;
    default:
      printf("\nEtrange...\n\n");
      break;
  }

  return liste;
}


/*
* Creer un maillon
*/
struct maillon* creerElement(struct maillon *nouveau_maillon)
{
  char saisie[100] = "";
  char nom[20] = "";
  char prenom[20] = "";
  char dateDernierPasse[20] = "Aucune";
  int numeroBadge = 0;
  char codeSecret[100] = "";


  printf("Nom de l'utilisateur : ");
  fgets(saisie, 20, stdin);
  saisie[strlen(saisie)-1] = '\0';
  strcpy( nom, saisie );

  printf("Prenom de l'utilisateur : ");
  fgets(saisie, 20, stdin);
  saisie[strlen(saisie)-1] = '\0';
  strcpy( prenom, saisie );

  do {
    if(numeroBadge < MIN_NUMERO_BADGE || numeroBadge > MAX_NUMERO_BADGE){
      printf(" Le code doit faire 4 chiffres %d et %d \n",MIN_NUMERO_BADGE ,MAX_NUMERO_BADGE );
    }

    printf("Numéro de badge : ");
    fgets(saisie, 10, stdin);
    numeroBadge = atoi(saisie);
  } while(numeroBadge > MAX_NUMERO_BADGE || numeroBadge < MIN_NUMERO_BADGE);

  printf("Le secret : ");
  fgets(saisie, 100, stdin);
  saisie[strlen(saisie)-1] = '\0';
  strcpy( codeSecret, saisie );

  strcpy( nouveau_maillon->personne.nom, nom );
  strcpy( nouveau_maillon->personne.prenom, prenom );
  strcpy( nouveau_maillon->personne.codeSecret, codeSecret );
  strcpy( nouveau_maillon->personne.dateDernierPasse, dateDernierPasse );
  nouveau_maillon->personne.numeroBadge = numeroBadge;

  nouveau_maillon->suivant = NULL;
  nouveau_maillon->precedent = NULL;

  return nouveau_maillon;
}

/*
* Permet d'ajouter un maillon à une liste
*/
struct maillon* ajouterMaillon(struct maillon *liste){
  struct maillon *tmp;
  struct maillon *nouveauMaillon = malloc(sizeof(*nouveauMaillon));
  creerElement(nouveauMaillon);

  //Si on a pas de maillon
  if (liste == NULL){
    liste = nouveauMaillon;
    return liste;
  }

  tmp = liste;
	while (tmp->suivant != NULL) {
		tmp = tmp->suivant;
	}

  nouveauMaillon->precedent = tmp;
  tmp->suivant = nouveauMaillon;

  return liste;
}

/*
* Affiche tous les elements d'une liste
*/
void afficherTousMaillons(struct maillon *liste){
  struct maillon *tmp = liste;

  if(liste == NULL){
    printf("Pas de maillon\n");
    return;
  }

  while (tmp != NULL) {
    printf("[Nom : %s, Prenom : %s, Code Secret : %s, Date Dernier Passage : %s, Numero Badge : %d]\n",tmp->personne.nom, tmp->personne.prenom, tmp->personne.codeSecret, tmp->personne.dateDernierPasse, tmp->personne.numeroBadge );
    tmp = tmp->suivant;
  }

}

struct maillon* supprimerPersonne(struct maillon *liste, int numeroBadge){
  struct maillon* precedent;
  struct maillon* tmp;

  //Si le premier element correspondnumeroBadge
  if (numeroBadge == liste->personne.numeroBadge) {
    precedent = liste;
    liste->precedent = NULL;
    liste = liste->suivant;
    free(precedent);
    return liste;
  }

  precedent = NULL;
  tmp = liste;
  while (tmp->suivant != NULL && numeroBadge == tmp->personne.numeroBadge) {
    precedent = tmp;
    tmp = tmp->suivant;
  }

  precedent = tmp;
  tmp = tmp->suivant;

  if (tmp != NULL) {
    precedent->suivant = tmp->suivant;
    if (tmp->suivant != NULL) {
      tmp->suivant->precedent = precedent;
    }
    free(tmp);
  } else {
    printf("Le numero de badge ne correspond pas\n");
  }

  return liste;

}

/*
* Fonction permettant de modifier le code secret de l'application
*/
struct maillon* modifierCodeSecret(struct maillon *liste, int numeroBadge){
  struct maillon* tmp;
  char codeSecret[100] = "";

  tmp = liste;

  while (tmp->suivant != NULL && numeroBadge != tmp->personne.numeroBadge) {
      tmp = tmp->suivant;
  }

  if (numeroBadge == tmp->personne.numeroBadge) {

    printf("Nouveau secret : ");
    fgets(codeSecret, 100, stdin);
    codeSecret[strlen(codeSecret)-1] = '\0';

    strcpy(tmp->personne.codeSecret, codeSecret);
  } else {
    printf("Le numero de badge ne correspond pas\n");
  }
  return liste;

}

/*
* Fonction permettant de simuler une authentification
*/
struct maillon* simulerControle (struct maillon* liste, char codeSecret[], int numeroBadge) {
  struct maillon* tmp;
  tmp = liste;
  time_t timestamp = time( NULL );
  struct tm * timeInfos = localtime( & timestamp );

  while (tmp->suivant != NULL && numeroBadge != tmp->personne.numeroBadge) {
      tmp = tmp->suivant;
  }

  if (strcmp(tmp->personne.codeSecret, codeSecret)) {
    printf("\nCode secret ou numéro de badge erroné\n");
  } else {
    printf("\nVous avez été connecté avec succès %s %s\n", tmp->personne.prenom, tmp->personne.nom);
    printf("Date actuelle mise à jour : %02d/%02d/%04d;%02d:%02d:%02d",
        timeInfos->tm_mday, timeInfos->tm_mon+1, timeInfos->tm_year+1900,
        timeInfos->tm_hour, timeInfos->tm_min, timeInfos->tm_sec);
    sprintf(tmp->personne.dateDernierPasse, "%02d/%02d/%04d;%02d:%02d:%02d",
        timeInfos->tm_mday, timeInfos->tm_mon+1, timeInfos->tm_year+1900,
        timeInfos->tm_hour, timeInfos->tm_min, timeInfos->tm_sec);
  }

  return liste;
}

/*
* Permet d'enregistrer la liste doublement chainee dans un fichier
*/
void enregistrerListeDansFichier(struct maillon* liste) {
  FILE* fichier;
  struct maillon* tmp;
  char pathFichier[150];

  tmp = liste;

  printf("Saisissez le path vers le fichier : ");
  fgets(pathFichier, 150, stdin);
  pathFichier[strlen(pathFichier)-1] = '\0';

  fichier = fopen(pathFichier, "w");

  if (fichier != NULL) {
    while (tmp != NULL) {
      fprintf(fichier, "%s %s %d %s %s ", tmp->personne.nom, tmp->personne.prenom,
                                      tmp->personne.numeroBadge, tmp->personne.codeSecret,
                                      tmp->personne.dateDernierPasse);
      tmp = tmp->suivant;
    }
    printf("Le fichier %s a été sauvegardé avec succès",pathFichier);
    fclose(fichier);
  } else {
    printf("Impossible d'ouvrir le fichier spécifié");
  }

}

struct maillon* chargerListeDansFichier(){
  FILE* fichier;
  struct maillon* liste = malloc(sizeof(struct maillon));
  struct maillon* tmp;
  struct maillon* precedent;
  char pathFichier[151];
  int ret;

  printf("Saisissez le path vers le fichier : ");
  fgets(pathFichier, 150, stdin);
  pathFichier[strlen(pathFichier)-1] = '\0';

  fichier = fopen(pathFichier, "r");

  if (fichier !=NULL) {
    tmp = liste;
    precedent = NULL;
    while ((ret = fscanf(fichier, "%s %s %d %s %s ", tmp->personne.nom, tmp->personne.prenom,
                                    &tmp->personne.numeroBadge, tmp->personne.codeSecret,
                                    tmp->personne.dateDernierPasse)) != EOF && ret != 0) {

      tmp->precedent = precedent;
      if (precedent != NULL) {
        precedent->suivant = tmp;
      }
      precedent = tmp;

      free(tmp);
      tmp = malloc(sizeof(struct maillon));
    }
    fclose(fichier);
    return liste;
  } else {
    printf("Le path vers le fichier n'existe pas\n");
  }
  return NULL;
}

int main() {

  int choix_menu = -1;
  int nbPersonnes = 0;

  struct maillon *liste = malloc(sizeof(*liste));
  liste = NULL;

  do {

    menu();
    choix_menu = lireChoix();
    if(choix_menu == 0)
    {
      break;
    }
    liste = executerChoix(choix_menu, liste);

  } while(choix_menu != 0);

  return 0;
}
