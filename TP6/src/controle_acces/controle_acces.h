int MAX_NUMERO_BADGE = 9999;
int MIN_NUMERO_BADGE = 0;


typedef struct{
  char nom[20];
  char prenom[20];
  char codeSecret[100];
  char dateDernierPasse[100];
  int numeroBadge;

} personne;

struct maillon {
	personne personne;
	struct maillon * suivant;
	struct maillon * precedent;
};


int lireChoix();
struct maillon* executerChoix(int choix, struct maillon *liste);
struct maillon* ajouterMaillon(struct maillon *liste);
void afficherTousMaillons(struct maillon *liste);
struct maillon* creerElement(struct maillon *nouveau_maillon);
struct maillon* supprimerPersonne(struct maillon *liste, int numeroBadge);
struct maillon* modifierCodeSecret(struct maillon *liste, int numeroBadge);
struct maillon* simulerControle (struct maillon* liste, char codeSecret[], int numeroBadge);
void enregistrerListeDansFichier(struct maillon* liste);
struct maillon* chargerListeDansFichier();
